var totalData = {};
var indexOutputsToCompanent = [];


require(["myVueWrapper",
    'Vue',
    'sweetAlert',
    'px-libs/select2.full',
    'px/extensions/bootstrap-datepicker'], function(myVueWrapper, Vue, sweetAlert) {

    myVueWrapper.loadCompanents(listComponents);

    apiRequests.getViewCollectionBySlug(prototypeMark1TopPanelSlug).then(function (data) {
        myVueWrapper.vueInit(function () {
            totalData = data;
            myVueWrapper.app = new Vue({
                el: "#prototypeMakr1BaseVueApp",
                data: {
                    panelData: totalData
                },
                methods: {
                    getOutputPanelItemBySlug: function (slug) {
                        for(var key in this.panelData.outputPanels) {
                            var item = this.panelData.outputPanels[key];

                            if(item.outputMappingPanel.slug === slug) {
                                return item;
                            }
                        }

                        return null;
                    },
                },
                created: function () {
                    initController();
                },
            });

            myVueWrapper.app.$on('base-change-event', function (eventData) {
                
                if(eventData.method === 'expParamChange') {
                    apiRequests.setOutputChangeValue(eventData.data).then(function (responseData) {
                        refreshChange(responseData);
                    });
                }

                if(eventData.method === 'sendMathEvent') {
                    apiRequests.mathEvent(eventData.data).then(function (responseData) {
                        refreshChange(responseData);
                    });
                }

                if(eventData.method === 'sendEventChange') {
                    apiRequests.sendEventChange(eventData.data).then(function (responseData) {
                        totalData.outputPanels = responseData.outputPanels;
                    });
                }

            });

            myVueWrapper.app.$on('load-show-expenses-modal', function (eventData) {
                expensesAddModalData.parentOutputMappingId = eventData.parentOutputMappingId
                apiRequests.getAvailableCategoryListByParentMapping(eventData.data).then(function (responseData) {
                    expensesAddModalData.outputMappingTree = responseData;
                    expensesAddModalData.update();
                });
                showModalExpensesAdd();
            });

            myVueWrapper.app.$on('create-new-expenses', function (eventData) {
                apiRequests.createNewExpenses(eventData.data).then(function (responseData) {
                    totalData.outputPanels = responseData.outputPanels;
                });
                hideModalExpensesAdd();
            });

        });
    });
});


function refreshChange(responseData) {
    if(responseData.changeExpParamList !== undefined) {
        for(var key in responseData.changeExpParamList) {
            var responseExpParam = responseData.changeExpParamList[key];
            changeExpParam(responseExpParam)
        }
    }
    if(responseData.changeOutputList !== undefined) {
        for(var key in responseData.changeOutputList) {
            var responseOutput = responseData.changeOutputList[key];
            changeOutput(responseOutput)
        }
    }
}


function changeExpParam(responseExpParam) {
    for(var i in indexOutputsToCompanent) {
        var indexOutput = indexOutputsToCompanent[i];

        if(indexOutput.item.expParam !== undefined && indexOutput.item.expParam !== null && indexOutput.item.expParam.id === responseExpParam.id) {
            indexOutput.item.expParam = responseExpParam;
            indexOutput.refrash();

        }
        if(indexOutput.item.expParamRelated !== undefined && indexOutput.item.expParamRelated !== null && indexOutput.item.expParamRelated.id === responseExpParam.id) {
            indexOutput.item.expParamRelated = responseExpParam;
            indexOutput.refrash();
        }
    }
}

function changeOutput(responseOutput) {
    for(var i in indexOutputsToCompanent) {
        var indexOutput = indexOutputsToCompanent[i];

        if(indexOutput.item.output.id === responseOutput.id) {
            setValueObjtctRecursive(responseOutput, indexOutput.item.output);
        }
    }
}


function setValueObjtctRecursive(source, target) {
    for (var key in source) {
        var itemSource = source[key];
        var itemTarget = target[key];

        if(typeof itemSource === "object" && itemSource !== null) {

            if( itemTarget === undefined || itemTarget === null) {
                if(Array.isArray(source) && Array.isArray(target)){
                    target.push(itemSource);
                    // console.log(['push', key, itemSource]);
                } else {
                    target[key] = itemSource;
                }
            } else {
                if(Array.isArray(itemSource) && Array.isArray(itemTarget) === false){
                    // console.log(['set array', key, itemSource]);
                    itemTarget = [];
                }
                setValueObjtctRecursive(itemSource, itemTarget);
            }
        } else {

            target[key] = itemSource;
        }
    }
}


function deleteValueObjtctRecursive(source, target) {
    for (var key in target) {
        var itemSource = source[key];
        var itemTarget = target[key];

        if(itemSource !== undefined) {
            if(typeof itemSource === "object" && itemSource !== null && Array.isArray(itemSource) === false ) {
                deleteValueObjtctRecursive(itemSource, itemTarget);
            } else if(Array.isArray(itemSource) && Array.isArray(itemTarget)) {
                deleteArrayObjtctRecursive(itemSource, itemTarget);
            } else {
                if(itemSource !== itemTarget) {
                    // console.log(['fff', key, itemSource, itemTarget]);
                }
                // delete itemTarget;
            }
        } else {
            // console.log(['delete', key, itemTarget]);
            delete itemTarget;
        }
    }
}


function deleteArrayObjtctRecursive(source, target) {
    for (var key in target) {
        var itemSource = source[key];
        var itemTarget = target[key];

        if(itemSource === undefined) {
            target.splice(-1, 1);
        } else {
            if(typeof itemSource === "object") {
                deleteValueObjtctRecursive(itemSource, itemTarget);
            }
        }
    }
}
