var apiRequests = function () {
    var self = this;
    self.urls = {};

    self.addUrl = function (name, url) {
        self.urls[name] = url;
    };

    self.getUrl = function (name) {
        return self.urls[name];
    };

    self.getBasicGetRequest = function (url, data, resolve, reject) {
        return {
            url: url,
            data: data,
            method: 'GET',
            success: function (data) {
                if (data.status !== undefined && data.status) {
                    resolve(data.data);
                } else {
                    reject(data);
                }
            },
            fail: function (data) {
                reject(data);
            }
        }
    };

    self.getBasicPostRequest = function (url, data, resolve, reject) {
        var basicPost = self.getBasicGetRequest(url, data, resolve, reject);
        basicPost.method = 'POST';
        return basicPost;
    };

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    self.getViewCollectionBySlug = function(slug) {
        return new Promise(function(resolve, reject){
            $.ajax(self.getBasicGetRequest(self.getUrl('getViewCollectionBySlug'), {slug:slug}, resolve, reject));
        });
    };

    self.setOutputChangeValue = function(data) {
        return new Promise(function(resolve, reject){
            $.ajax(self.getBasicPostRequest(self.getUrl('setOutputChangeValue'), data, resolve, reject));
        });
    };

    self.sendEventChange = function(data) {
        return new Promise(function(resolve, reject){
            $.ajax(self.getBasicPostRequest(self.getUrl('sendEventChange'), data, resolve, reject));
        });
    };

    self.deleteCostValueById = function(slug) {
        return new Promise(function(resolve, reject){
            $.ajax(self.getBasicGetRequest(self.getUrl('getViewCollectionBySlug'), {slug:slug}, resolve, reject));
        });
    };

    self.getAvailableCategoryListByParentMapping = function(data) {
        return new Promise(function(resolve, reject){
            $.ajax(self.getBasicGetRequest(self.getUrl('getAvailableCategoryListByParentMapping'), data, resolve, reject));
        });
    };

    self.createNewExpenses = function(data) {
        return new Promise(function(resolve, reject){
            $.ajax(self.getBasicGetRequest(self.getUrl('createNewExpenses'), data, resolve, reject));
        });
    };

    self.mathEvent = function(data) {
        return new Promise(function(resolve, reject){
            $.ajax(self.getBasicGetRequest(self.getUrl('mathEvent'), data, resolve, reject));
        });
    };

    self.getTemplate = function(data) {
        return new Promise(function(resolve, reject){
            $.ajax({
                url: self.getUrl('getTemplate'),
                data: data,
                method: 'GET',
                success: function (data) {
                    resolve(data);
                },
                fail: function (data) {
                    reject(data);
                }
            });
        });
    };

    return self;
}();
