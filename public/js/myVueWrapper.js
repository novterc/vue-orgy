define(['Vue'], function(Vue) {
    var myVueWrapperClass = function () {
        var myVueWrapper = this;
        myVueWrapper.prefixCompanentPath = null;
        myVueWrapper.initCallback = null;
        myVueWrapper.initCompanentInc = 0;
        myVueWrapper.noVueInit = true;

        myVueWrapper.vueInit = function (initCallback) {
            myVueWrapper.initCallback = initCallback;
            if(myVueWrapper.initCompanentInc === 0 && myVueWrapper.noVueInit) {
                myVueWrapper.noVueInit = false;
                myVueWrapper.initCallback();
            }
        };

        myVueWrapper.addCompanent = function (templatePath, initCallbackCompnanent) {
            apiRequests.getTemplate({ templatePath:templatePath }).then(function (templateHtml) {
                initCallbackCompnanent(templateHtml);
                myVueWrapper.initCompanentInc--;
                if(myVueWrapper.initCompanentInc === 0 && myVueWrapper.initCallback !== null && myVueWrapper.noVueInit) {
                    myVueWrapper.noVueInit = false;
                    myVueWrapper.initCallback();
                }
            });
        };

        myVueWrapper.loadCompanents = function (paths) {
            paths.forEach(function(item, i, paths) {
                myVueWrapper.initCompanentInc++;
                paths[i] = ( myVueWrapper.prefixCompanentPath + item );
            });
            require(paths, function() {});
        };

        return myVueWrapper;
    };

    return new myVueWrapperClass();
});