require(['Vue','myVueWrapper', '/bundles/app/prototypeMark1Vue/outputBaseCompanent.js'], function(Vue, myVueWrapper, baseOutput){
    myVueWrapper.addCompanent('outputBlockTemplate', function(template){

        var componentObj = baseOutput.getCompanentObj(template);
        Vue.component("output-block", componentObj);

    });
});