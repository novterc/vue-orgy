require(['Vue','myVueWrapper'], function(Vue, myVueWrapper){
    myVueWrapper.addCompanent('panelBlockTemplate', function(template){

        Vue.component("panel-block", {
            template: template,
            props: ['output-panel', 'deep-level'],
            data: function() {
                return {
                };
            },
            created: function() {},
            methods: {

                getChildDeepLevel: function () {
                    return (this.deepLevel + 1);
                },

                getOuputItemBySlug: function (slug) {
                    for(var key in this.outputPanel.outputs) {
                        var item = this.outputPanel.outputs[key];

                        if(item.outputMapping.slug === slug){
                            console.log(slug,item);
                            return item;
                        }
                    }

                    return null;
                },

                getOutputItemBySlugWithDeafultValue: function (slug,defaultValue){
                    var val = this.getOuputItemBySlug(slug);
                    if (val !== null
                        && typeof(val.expParam.ptdCustom.view) !== "undefined"
                        && val.expParam.ptdCustom.view !== null
                        && val.expParam.ptdCustom.view !== "?"
                    ){
                        var value = val.expParam.ptdCustom.view.name;
                        if (typeof(val.expParam.ptdCustom.view.attributes) !== "undefined"
                            && typeof(val.expParam.ptdCustom.view.attributes.measure) !== "undefined"
                            && val.expParam.ptdCustom.view.attributes.measure !== null
                        ){
                            value += ' <span class="font-weight-normal">(' + val.expParam.ptdCustom.view.attributes.measure + ')</span>';
                        }

                        return value
                    }

                    return defaultValue;
                },


                getAttributes: function (attrname) {
                    if(this.outputPanel.outputMappingPanel.attributes === undefined || this.outputPanel.outputMappingPanel.attributes === null) {
                        return null;
                    }
                    if(this.outputPanel.outputMappingPanel.attributes[attrname] === undefined) {
                        return null;
                    }

                    return this.outputPanel.outputMappingPanel.attributes[attrname];
                }

            }
        });

    });
});