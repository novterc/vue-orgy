require(['Vue','myVueWrapper', '/bundles/app/prototypeMark1Vue/outputBaseCompanent.js', ],
    function(Vue, myVueWrapper, baseOutput){

    myVueWrapper.addCompanent('graphs/countWellsYearsTemplate', function(template){

        console.log('load output-graph-count-wells-years')
        var componentObj = baseOutput.getCompanentObj(template);
        var parentCreate = componentObj.created;
        componentObj.created = function () {
            thisComponentObj = this;
            parentCreate.call(thisComponentObj);

            console.log('created output-graph-count-wells-years ' + this._uid);

            thisComponentObj.thisGraph = null;
            thisComponentObj.graphData = [];
            thisComponentObj.randGraphNumber = makeid()
        };

        componentObj.methods.getGraphDomId = function () {
            return 'chartdiv_graph_' + this._uid;
        };

        var parentMounted = componentObj.mounted;
        componentObj.mounted = function () {
            var thisComponentObj = this;
            parentMounted.call(thisComponentObj);

            if(thisComponentObj.item.expParam.ptdCustom.error === null) {
                thisComponentObj.graphData = thisComponentObj.item.expParam.ptdCustom.view;
            } else {
                console.log('ERROR expParam.ptdCustom data');
            }

            thisComponentObj.thisGraph = initGraph(thisComponentObj.graphData, thisComponentObj.getGraphDomId());
        };

        var parentRefrash = componentObj.methods.refrash;
        componentObj.methods.refrash = function () {
            var thisComponentObj = this;
            parentRefrash.call(thisComponentObj);

            if(thisComponentObj.item.expParam.ptdCustom.error === null) {
                thisComponentObj.graphData = thisComponentObj.item.expParam.ptdCustom.view;
            } else {
                thisComponentObj.graphData = [];
                console.log('ERROR expParam.ptdCustom data');
            }

            $(document).ready(function () {
                if(thisComponentObj.thisGraph !== null) {
                    thisComponentObj.thisGraph.dataProvider = thisComponentObj.graphData;
                    thisComponentObj.thisGraph.validateData();
                }
            });
        };

        Vue.component("output-graph-count-wells-years", componentObj);
    });

    function makeid() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 5; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        }

    function initGraph(chartData, domId) {
        var graphItem = AmCharts.makeChart(domId, {
            "type": "serial",
            "theme": "light",
            "language": "ru",
            "showAll": true,
            "dataProvider": chartData,
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left",
                "title": "Скважин, шт.",
                "minimum": 0
            }],
            "balloon": {
                "borderThickness": 1,
                "shadowAlpha": 0
            },
            "graphs": [{
                "balloonText": "<b>[[year]] г.</b>: [[quantity]] скв.",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "quantity",
                "lineColor": "#199ED9",
            }],
            "chartScrollbar": {
                "dragIcon": "dragIconRectBig",
                "dragIconHeight": 30,
                "oppositeAxis":false,
                "offset":40,
                "selectedBackgroundAlpha": 0.4,
                "selectedBackgroundColor": "#67b7dc",
                "scrollbarHeight":10
            },
            "chartCursor": {
                "cursorAlpha": 0.2,
                "cursorColor": "#258cbb",
                "fullWidth": false,
                "valueBalloonsEnabled": true,
                "zoomable": true,
            },
            "categoryField": "year",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
            },
        });

        AmCharts.translations[ "export" ][ "en" ]={"zoomOutText":"Показать все"};
        AmCharts.zoomOutText = "Показать все";

        zoomChart();

        function zoomChart() {
            graphItem.zoomToIndexes(graphItem.dataProvider.length - 40, graphItem.dataProvider.length - 1);
        }

        return graphItem;

    }

});