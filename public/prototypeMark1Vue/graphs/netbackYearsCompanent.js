require(['Vue','myVueWrapper', '/bundles/app/prototypeMark1Vue/outputBaseCompanent.js', ],
    function(Vue, myVueWrapper, baseOutput){

    myVueWrapper.addCompanent('graphs/netbackYearsTemplate', function(template){

        var componentObj = baseOutput.getCompanentObj(template);
        var parentCreate = componentObj.created;
        var thisComponentObj = null;
        var thisGraph = null;
        componentObj.created = function () {
            thisComponentObj = this;
            parentCreate.call(thisComponentObj);
            console.log('init output-graph-netback-years');
            console.log(this.item.expParam);
        };

        componentObj.refrashCustom = function () {
            $(document).ready(function () {
                var graphData = [];
                if(thisComponentObj.item.expParam.ptdCustom.error === null) {
                    graphData = thisComponentObj.item.expParam.ptdCustom.view;
                }

                if(thisGraph === null) {
                    thisGraph = initGraph(graphData);
                } else {
                    thisGraph.dataProvider = graphData;
                    thisGraph.validateData();
                }
            });
        };

        Vue.component("output-graph-netback-years", componentObj);
    });

    function initGraph(chartData) {

        var priceOil = 0;
        if (chartData && chartData[0] && chartData[0]['priceOil']){
            priceOil = chartData[0]['priceOil'];
        }

        return AmCharts.makeChart("chartdiv_graph2", {
            "type": "serial",
            "theme": "light",
            "language": "ru",
            "showAll": true,
            "dataProvider": chartData,
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left",
                "title": "руб./т",
                "minimum": 0,
                "guides": [{
                    "inside": true,
                    "label": "Рекомендуемое значение нетбека",
                    "lineAlpha": 1,
                    "value": priceOil,
                    "lineColor": "#F15E21",
                    "lineThickness": 2,
                    "position": "right",
                    "fontSize": 13,
                    "above": true,
                    "tickLength": 10,
                }]
            }],
            "balloon": {
                "borderThickness": 1,
                "shadowAlpha": 0
            },
            "graphs": [{
                "balloonText": "<b>[[date]] месяц, год</b>: [[value]] руб./т.",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "value",
                "lineColor": "#199ED9",
            }],
            "chartScrollbar": {
                "dragIcon": "dragIconRectBig",
                "dragIconHeight": 30,
                "oppositeAxis":false,
                "offset":40,
                "selectedBackgroundAlpha": 0.4,
                "selectedBackgroundColor": "#67b7dc",
                "scrollbarHeight":10
            },
            "chartCursor": {
                "cursorAlpha": 0.2,
                "cursorColor": "#258cbb",
                "fullWidth": false,
                "valueBalloonsEnabled": true,
                "zoomable": true,
            },
            "categoryField": "date",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
            },
        });
    }

});