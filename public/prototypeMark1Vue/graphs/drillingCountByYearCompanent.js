require(['Vue','myVueWrapper', '/bundles/app/prototypeMark1Vue/outputBaseCompanent.js', ],
    function(Vue, myVueWrapper, baseOutput){

    myVueWrapper.addCompanent('graphs/countWellsYearsTemplate', function(template){

        console.log('load graph-drilling-by-year');
        var componentObj = baseOutput.getCompanentObj(template);
        var parentCreate = componentObj.created;
        componentObj.created = function () {
            thisComponentObj = this;
            parentCreate.call(thisComponentObj);

            console.log('created output-graph-drilling-by-year ' + this._uid);

            thisComponentObj.thisGraph = null;
            thisComponentObj.graphData = [];
            thisComponentObj.randGraphNumber = makeid()
        };

        componentObj.methods.getGraphDomId = function () {
            return 'chartdiv_graph_' + this._uid;
        };

        var parentMounted = componentObj.mounted;
        componentObj.mounted = function () {
            var thisComponentObj = this;
            parentMounted.call(thisComponentObj);

            if(thisComponentObj.item.expParam.ptdCustom.error === null) {
                thisComponentObj.graphData = thisComponentObj.item.expParam.ptdCustom.view;
            } else {
                console.log('ERROR expParam.ptdCustom data');
            }

            thisComponentObj.thisGraph = initGraph(thisComponentObj.graphData, thisComponentObj.getGraphDomId());
        };

        var parentRefrash = componentObj.methods.refrash;
        componentObj.methods.refrash = function () {
            var thisComponentObj = this;
            parentRefrash.call(thisComponentObj);

            if(thisComponentObj.item.expParam.ptdCustom.error === null) {
                thisComponentObj.graphData = thisComponentObj.item.expParam.ptdCustom.view;
            } else {
                thisComponentObj.graphData = [];
                console.log('ERROR expParam.ptdCustom data');
            }

            $(document).ready(function () {
                if(thisComponentObj.thisGraph !== null) {
                    thisComponentObj.thisGraph.dataProvider = thisComponentObj.graphData;
                    thisComponentObj.thisGraph.validateData();
                }
            });
        };

        Vue.component("output-graph-drilling-by-year", componentObj);
    });

    function makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    function formatNumber(value) {
        return Math.round( value * 10 ) / 10
    }

    function initGraph(chartData, domId) {
        var graphItem = AmCharts.makeChart(domId, {
            "type": "serial",
            "theme": "light",
            "language": "ru",
            "showAll": true,
            "legend": {
                "align": "center",
                "equalWidths": false,
                "useGraphSettings": true,
                "markerSize": 10,
                "valueFunction": function(graphDataItem, valueText) {
                    return "";
                }
            },
            "dataProvider": chartData,
            "numberFormatter": {
                "precision": -1,
                "decimalSeparator": ",",
                "thousandsSeparator": " "
            },
            "valueAxes": [{
                "stackType": "regular",
            }],
            "balloon": {
                "maxWidth": 300,
                "borderThickness": 1,
                "shadowAlpha": 0
            },
            "graphs": [{
                "balloonFunction": function(item) { return "<b>Добывающие</b>: " + formatNumber(item.values.value) + " скв."; },
                "fillAlphas": 0.9,
                "lineAlpha": 0.4,
                "type": "column",
                "valueField": "value1",
                "fillColors": "#137EC3",
                "lineColor": "#000000",
                "title": "Добывающие",
            },{
                "balloonFunction": function(item) { return "<b>Нагнетательные</b>: " + formatNumber(item.values.value) + " скв."; },
                "fillAlphas": 0.9,
                "lineAlpha": 0.4,
                "type": "column",
                "valueField": "value2",
                "lineColor": "#000000",
                "title": "Нагнетательные",
                "fillColors": "#114773",

            }],
            "chartScrollbar": {
                "dragIcon": "dragIconRectBig",
                "dragIconHeight": 30,
                "oppositeAxis":false,
                "offset":40,
                "selectedBackgroundAlpha": 0.4,
                "selectedBackgroundColor": "#67b7dc",
                "scrollbarHeight":10
            },
            "chartCursor": {
                "cursorAlpha": 0.2,
                "cursorColor": "#258cbb",
                "fullWidth": false,
                "valueBalloonsEnabled": true,
                "zoomable": true,
            },
            "categoryField": "date",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
            },
        });

        AmCharts.translations[ "export" ][ "en" ]={"zoomOutText":"Показать все"};
        AmCharts.zoomOutText = "Показать все";

        zoomChart();

        function zoomChart() {
            graphItem.zoomToIndexes(graphItem.dataProvider.length - 40, graphItem.dataProvider.length - 1);
        }

        return graphItem;

    }

});