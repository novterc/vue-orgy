require(['Vue','myVueWrapper', '/bundles/app/prototypeMark1Vue/outputBaseCompanent.js', ],
    function(Vue, myVueWrapper, baseOutput){

    myVueWrapper.addCompanent('graphs/countWellsYearsTemplate', function(template){

        console.log('load output-graph-irr');
        var componentObj = baseOutput.getCompanentObj(template);
        var parentCreate = componentObj.created;
        componentObj.created = function () {
            thisComponentObj = this;
            parentCreate.call(thisComponentObj);

            console.log('created output-graph-irr ' + this._uid);

            thisComponentObj.thisGraph = null;
            thisComponentObj.graphData = [];
            thisComponentObj.randGraphNumber = makeid()
        };

        componentObj.methods.getGraphDomId = function () {
            return 'chartdiv_graph_' + this._uid;
        };

        var parentMounted = componentObj.mounted;
        componentObj.mounted = function () {
            var thisComponentObj = this;
            parentMounted.call(thisComponentObj);

            if(thisComponentObj.item.expParam.ptdCustom.error === null) {
                thisComponentObj.graphData = thisComponentObj.item.expParam.ptdCustom.view;
            } else {
                console.log('ERROR expParam.ptdCustom data');
            }

            thisComponentObj.thisGraph = initGraph(thisComponentObj.graphData, thisComponentObj.getGraphDomId());
        };

        var parentRefrash = componentObj.methods.refrash;
        componentObj.methods.refrash = function () {
            var thisComponentObj = this;
            parentRefrash.call(thisComponentObj);

            if(thisComponentObj.item.expParam.ptdCustom.error === null) {
                thisComponentObj.graphData = thisComponentObj.item.expParam.ptdCustom.view;
            } else {
                thisComponentObj.graphData = [];
                console.log('ERROR expParam.ptdCustom data');
            }

            $(document).ready(function () {
                if(thisComponentObj.thisGraph !== null) {
                    thisComponentObj.thisGraph.dataProvider = thisComponentObj.graphData;
                    thisComponentObj.thisGraph.validateData();
                }
            });
        };

        Vue.component("output-graph-irr", componentObj);
    });

    function makeid() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 5; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        }

    function initGraph(chartData, domId) {
        var graphItem = AmCharts.makeChart(domId, {
            "type": "serial",
            "theme": "light",
            "language": "ru",
            "showAll": true,
            "minMarginBottom": 30,
            "dataProvider": chartData,
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left",
                "title": "%",
                "minimum": 0,
                //"maximum": 100,
                "guides": [{
                    "inside": true,
                    "label": "Ставка дисконтирования: 15%",
                    "lineAlpha": 1,
                    "value": 15,
                    "lineColor": "#F15E21",
                    "lineThickness": 2,
                    "position": "right",
                    "fontSize": 13,
                    "above": true,
                    "tickLength": 10,
                }],
            }],
            "balloon": {
                "borderThickness": 1,
                "shadowAlpha": 0
            },
            "graphs": [{
                "balloonText": "<b>Номер скважины</b>: [[well]]<br /><b>IRR, % (Проект)</b>: [[irr]]",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "irr",
                "lineColor": "#199ED9",
                "negativeBase": 15,
                "negativeFillColors": "#F15E21",
            }],
            "chartScrollbar": {
                "dragIcon": "dragIconRectBig",
                "dragIconHeight": 30,
                "oppositeAxis":false,
                "offset":40,
                "selectedBackgroundAlpha": 0.4,
                "selectedBackgroundColor": "#67b7dc",
                "scrollbarHeight":10
            },
            "chartCursor": {
                "cursorAlpha": 0.2,
                "cursorColor": "#258cbb",
                "fullWidth": false,
                "valueBalloonsEnabled": true,
                "zoomable": true,
            },
            "categoryField": "well",
            "categoryAxis": {
                "labelsEnabled": false,
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
            },
        });

        AmCharts.translations[ "export" ][ "en" ]={"zoomOutText":"Показать все"};
        AmCharts.zoomOutText = "Показать все";

        zoomChart();

        function zoomChart() {
            graphItem.zoomToIndexes(graphItem.dataProvider.length - 40, graphItem.dataProvider.length - 1);
        }

        return graphItem;

    }

});