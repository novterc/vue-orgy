require(['Vue','myVueWrapper', '/bundles/app/prototypeMark1Vue/outputBaseCompanent.js', ],
    function(Vue, myVueWrapper, baseOutput){

    myVueWrapper.addCompanent('graphs/oilPricesDollarRateTemplate', function(template){

        var componentObj = baseOutput.getCompanentObj(template);
        var parentCreate = componentObj.created;
        var thisComponentObj = null;
        var thisGraph = null;
        componentObj.created = function () {

            thisComponentObj = this;
            parentCreate.call(thisComponentObj);
        };

        componentObj.refrashCustom = function () {
            $(document).ready(function () {

                var graphData = [];
                if(thisComponentObj.item.expParam.ptdCustom.error === null) {
                    graphData = thisComponentObj.item.expParam.ptdCustom.view;
                }

                if(thisGraph === null) {
                    thisGraph = initGraph(graphData);
                } else {
                    thisGraph.dataProvider = graphData;
                    thisGraph.validateData();
                }
            });
        };

        Vue.component("output-graph-oil-price-dollar-rate", componentObj);
    });

    function initGraph(chartDataPrices) {
        return AmCharts.makeChart("chartdiv_prices", {
            "type": "xy",
            "theme": "light",
            "language": "ru",
            "showAll": true,
            "dataProvider": chartDataPrices,
            "dataDateFormat": "YYYY-MM-DD",
            //"hideYScrollbar":true,
            "trendLines": [],
            "valueAxes": [{
                "id": "ValueAxis-1",
                "axisAlpha": 0,
                "title": "Цена на нефть Юралс, долл./барр.",
                "titleBold": false,
                "includeAllValues":true,
            },{
                "id": "ValueAxis-2",
                "position": "bottom",
                "axisAlpha": 0,
                "title": "Курс доллара, руб./долл.",
                "titleBold": false,
            }, {
                "id": "ValueAxis-3",
                "axisAlpha": 0,
                "position": "bottom",
                "type": "date",
                "minimumDate": new Date(2014, 11, 31),
                "maximumDate": new Date(2015, 0, 13)
            }],
            "graphs": [{
                "id": "g1",
                "balloonText": "Цена на нефть Юралс: <b>[[oil]]</b><br />Курс доллара: <b>[[rate]]</b><br />На дату: <b>[[date]]</b>",
                "bullet": "bubble",
                "lineAlpha": 0,
                "valueField": 1,
                "xField": "rate",
                "yField": "oil",
                "bulletBorderAlpha": 0.1,
                'bulletBorderColor': '#ffffff',
                'bulletBorderThickness': 0,
                'bulletSize': 8,
                'bulletColor': '#007DC3',
                'bulletAlpha' : 0.7,
            }
                ,{
                    "id": "g2",
                    "balloonText": "Выбранное значение. <br /> Цена на нефть Юралс: <b>[[oil2]]</b><br />Курс доллара: <b>[[rate2]]</b>",
                    "bullet": "bubble",
                    "lineAlpha": 0,
                    "valueField": 1,
                    "xField": "rate2",
                    "yField": "oil2",
                    "bulletBorderAlpha": 0.1,
                    'bulletBorderColor': '#EF5F2F',
                    'bulletBorderThickness': 0,
                    'bulletSize': 8,
                    'bulletColor': '#EF5F2F',
                    'bulletAlpha' : 0.7,
                }],
            "marginLeft": 46,
            "marginBottom": 35,
            "chartCursor": {
                "cursorColor":"#258cbb",
            },

            /*
           "chartScrollbar": {
               "dragIcon": "dragIconRectBig",
               "dragIconHeight": 30,
               "oppositeAxis":false,
               "offset":40,
               "selectedBackgroundAlpha": 0.4,
               "selectedBackgroundColor": "#67b7dc",
               "scrollbarHeight":10
           },
           */

            "balloon":{
                "fixedPosition":true,
                "borderThickness": 1,
                "shadowAlpha": 0
            },

        });
    }
});