require(['Vue','myVueWrapper', '/bundles/app/prototypeMark1Vue/outputBaseCompanent.js', ],
    function(Vue, myVueWrapper, baseOutput){

    myVueWrapper.addCompanent('graphs/countWellsYearsTemplate', function(template){

        console.log('load output-graph-debit-first-years');
        var componentObj = baseOutput.getCompanentObj(template);
        var parentCreate = componentObj.created;
        componentObj.created = function () {
            thisComponentObj = this;
            parentCreate.call(thisComponentObj);

            console.log('created output-graph-debit-first-years ' + this._uid);

            thisComponentObj.thisGraph = null;
            thisComponentObj.graphData = [];
            thisComponentObj.randGraphNumber = makeid()
        };

        componentObj.methods.getGraphDomId = function () {
            return 'chartdiv_graph_' + this._uid;
        };

        var parentMounted = componentObj.mounted;
        componentObj.mounted = function () {
            var thisComponentObj = this;
            parentMounted.call(thisComponentObj);

            if(thisComponentObj.item.expParam.ptdCustom.error === null) {
                thisComponentObj.graphData = thisComponentObj.item.expParam.ptdCustom.view;
            } else {
                console.log('ERROR expParam.ptdCustom data');
            }

            thisComponentObj.thisGraph = initGraph(thisComponentObj.graphData, thisComponentObj.getGraphDomId());
        };

        var parentRefrash = componentObj.methods.refrash;
        componentObj.methods.refrash = function () {
            var thisComponentObj = this;
            parentRefrash.call(thisComponentObj);

            if(thisComponentObj.item.expParam.ptdCustom.error === null) {
                thisComponentObj.graphData = thisComponentObj.item.expParam.ptdCustom.view;
            } else {
                thisComponentObj.graphData = [];
                console.log('ERROR expParam.ptdCustom data');
            }

            $(document).ready(function () {
                if(thisComponentObj.thisGraph !== null) {
                    thisComponentObj.thisGraph.dataProvider = thisComponentObj.graphData;
                    thisComponentObj.thisGraph.validateData();
                }
            });
        };

        Vue.component("output-graph-debit-first-years", componentObj);
    });

    function makeid() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 5; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        }

    function initGraph(chartData, domId) {
        var graphItem = AmCharts.makeChart(domId, {
            "type": "serial",
            "theme": "light",
            "language": "ru",
            "showAll": true,
            "legend": {
                "enabled" : false,
                "align": "center",
                "equalWidths": false,
                "useGraphSettings": true,
                "markerSize": 10,
                "valueFunction": function(graphDataItem, valueText) {
                    return "";
                }
            },
            "dataProvider": chartData,
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left",
                "title": "Количество скважин, шт.",
                "minimum": 0,
                "stackType": "regular",
            }],
            "balloon": {
                "borderThickness": 1,
                "shadowAlpha": 0
            },
            "graphs": [
                // {
                // "balloonText": "<b>Нерентабельные скважины</b>: [[norent]]",
                // "fillAlphas": 0.9,
                // "lineAlpha": 0.2,
                // "type": "column",
                // "valueField": "norent",
                // "labelText": "[[norent]]",
                // "lineColor": "#199ED9",
                // "title": "Нерентабельные скважины",
                // },
                {
                    //"balloonText": "<b>Рентабельные скважины</b>: [[rent]]",
                    "balloonText": "<b>Количество скважин, шт.</b>: [[rent]]",
                    "fillAlphas": 0.9,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "rent",
                    "labelText": "[[rent]]",
                    "lineColor": "#F15E21",
                    //"title": "Рентабельные скважины",
                }
            ],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "range",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
                "title": "Диапазон дебита, т/сут.",
            },
        });

        AmCharts.translations[ "export" ][ "en" ]={"zoomOutText":"Показать все"};
        AmCharts.zoomOutText = "Показать все";

        zoomChart();

        function zoomChart() {
            graphItem.zoomToIndexes(graphItem.dataProvider.length - 40, graphItem.dataProvider.length - 1);
        }

        return graphItem;

    }

});