require(['Vue','myVueWrapper'], function(Vue, myVueWrapper){
    myVueWrapper.addCompanent('expensesPopUp', function(template){

        Vue.component("expenses-popup", {
            template: template,
            props: ['modal-data'],
            data: function() {
                return {
                    firstOutputMappingList: [],
                    firstSelectedOutputMapping: null,

                    secondOutputMappingList: [],
                    secondSelectedOutputMapping: null,

                    measureList: [],
                    selectedMeasure: null,

                    expensesValue: null
                };
            },
            created: function() {
                console.log('expensesPopUpCompanent created');
                this.modalData.update = this.updateModalData
            },
            methods: {
                updateModalData: function () {
                    this.firstOutputMappingList = this.modalData.outputMappingTree;
                    this.firstSelectedOutputMapping = null;

                    this.secondOutputMappingList = [];
                    this.secondSelectedOutputMapping = null;

                    this.measureList = [];
                    this.selectedMeasure = null;

                    this.expensesValue = null;
                },

                changeFirstSelect: function () {
                    if( this.firstSelectedOutputMapping !== null && this.firstSelectedOutputMapping.childs !== undefined) {
                        this.secondOutputMappingList = this.firstSelectedOutputMapping.childs;
                        this.measureList = [];
                        this.selectedMeasure = null;
                        this.expensesValue = null;
                    } else {
                        this.secondOutputMappingList = [];
                        this.measureList = [];
                        this.selectedMeasure = null;
                        this.expensesValue = null;
                    }
                },

                changeSecondSelect: function () {
                    if( this.secondSelectedOutputMapping !== null && this.secondSelectedOutputMapping.measures !== undefined) {
                        this.measureList = this.secondSelectedOutputMapping.measures
                        this.selectedMeasure = null;
                        this.expensesValue = null;
                    } else {
                        this.measureList = [];
                        this.selectedMeasure = null;
                        this.expensesValue = null;
                    }
                },

                onSubmit: function () {
                    myVueWrapper.app.$emit('create-new-expenses', {
                        data: {
                            slug: prototypeMark1TopPanelSlug,
                            outputMappingId: this.secondSelectedOutputMapping.outputMapping.id,
                            measureId: this.selectedMeasure.id,
                            value: this.expensesValue
                        }
                    });
                }
            }
        });

    });
});