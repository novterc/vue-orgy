require(['Vue','myVueWrapper'], function(Vue, myVueWrapper){
    myVueWrapper.addCompanent('panelTemplate', function(template){

        Vue.component("panel", {
            template: template,
            props: ['output-panel'],
            data: function() {
                return {};
            },
            created: function() {},
            mounted: function () {},
            updated: function () {},
            methods: {}
        });

    });
});