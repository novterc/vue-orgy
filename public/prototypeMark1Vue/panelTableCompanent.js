require(['Vue','myVueWrapper'], function(Vue, myVueWrapper){
    myVueWrapper.addCompanent('panelTableTemplate', function(template){

        Vue.component("panel-table", {
            template: template,
            props: ['output-panel'],
            data: function() {
                return {};
            },
            created: function() {
                console.log('panelTable Components created');
            },
            mounted: function () {
                console.log('panelTable Components mounted');
            },
            methods: {


                getAttributes: function (attrname) {
                    if(this.outputPanel.outputMappingPanel.attributes === undefined || this.outputPanel.outputMappingPanel.attributes === null) {
                        return null;
                    }
                    if(this.outputPanel.outputMappingPanel.attributes[attrname] === undefined) {
                        return null;
                    }

                    return this.outputPanel.outputMappingPanel.attributes[attrname];
                }

            }
        });

    });
});