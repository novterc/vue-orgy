require(['Vue','myVueWrapper', '/bundles/app/prototypeMark1Vue/outputBaseCompanent.js'], function(Vue, myVueWrapper, baseOutput){
    myVueWrapper.addCompanent('outputs/eventClickTemplate', function(template){

        Vue.component("output-event-click", {
            template: template,
            props: ['item', 'deepLebel', 'childCount', 'parentCheat'],

            data: function () {
                return {
                    model: {
                        slugSelectedType: false,
                        isDanger: false,
                        isWarning: false,
                        inputSelected: null
                    }
                };
            },

            created: function () {
                indexOutputsToCompanent.push(this);
            },

            refrash: function () {},

            methods: {
                getAttributes: function (attrname) {
                    if (this.item.outputMapping === undefined) {
                        return null;
                    }
                    if (this.item.outputMapping.attributes === undefined || this.item.outputMapping.attributes === null) {
                        return null;
                    }
                    if (this.item.outputMapping.attributes[attrname] === undefined) {
                        return null;
                    }

                    return this.item.outputMapping.attributes[attrname];
                },

                getClass: function () {
                    return this.getAttributes('class');
                },

                onClick: function () {
                    if(this.getAttributes('totalChange')) {
                        myVueWrapper.app.$emit('base-change-event', {
                            method: 'sendEventChange',
                            data: {
                                sendEventName: 'runMathMethod',
                                mathMethodId: this.getAttributes('mathMethodId'),
                                slug: prototypeMark1TopPanelSlug,
                            }
                        });
                    } else {
                        myVueWrapper.app.$emit('base-change-event', {
                            method: 'sendMathEvent',
                            data: {
                                sendEventName: 'runMathMethod',
                                mathMethodId: this.getAttributes('mathMethodId'),
                            }
                        });
                    }
                }
            }

        });

    });
});