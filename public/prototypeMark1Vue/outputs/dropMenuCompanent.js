require(['Vue','myVueWrapper', '/bundles/app/prototypeMark1Vue/outputBaseCompanent.js'], function(Vue, myVueWrapper, baseOutput){
    myVueWrapper.addCompanent('outputs/dropMenuTemplate', function(template){

        var componentObj = baseOutput.getCompanentObj(template);
        var parentCreate = componentObj.created;
        var thisComponentObj = null;
        componentObj.created = function () {
            thisComponentObj = this;
            parentCreate.call(thisComponentObj);
        };

        var parentMounted = componentObj.mounted;
        componentObj.mounted = function () {
            parentMounted.call(thisComponentObj);
        };

        Vue.component("output-block-drop-menu", componentObj);

    });
});