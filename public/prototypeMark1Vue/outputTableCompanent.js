require(['Vue','myVueWrapper', '/bundles/app/prototypeMark1Vue/outputBaseCompanent.js'], function(Vue, myVueWrapper, baseOutput){
    myVueWrapper.addCompanent('outputTableTemplate', function(template){

        var componentObj = baseOutput.getCompanentObj(template);
        Vue.component("output-table", componentObj);

    });
});