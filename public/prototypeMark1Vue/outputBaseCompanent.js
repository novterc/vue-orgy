define(['myVueWrapper', 'sweetAlert'], function(myVueWrapper, sweetAlert) {
    return {
        getCompanentObj: function (template) {
            var componentBaseObj = {
                template: template,
                props: ['item', 'deepLebel', 'childCount', 'parentCheat'],
                data: function () {
                    return {
                        model: {
                            slugSelectedType: false,
                            isDanger: false,
                            isWarning: false,
                            inputSelected: null
                        }
                    };
                },
                created: function () {
                    indexOutputsToCompanent.push(this);
                    this.refrash(this);
                },
                mounted: function () {
                    var that = this;

                    if(typeof this.getClass() === 'string' && this.getClass().indexOf('select2') !== -1) {
                        var $sel2 = $(this.$el).find('.select2');
                        if ($sel2.length > 0) {
                            $sel2.select2({
                                language: {
                                    noResults: function () {
                                        return 'Совпадений не найдено';
                                    }
                                }
                            }).on('change', function (e) {
                                that.item.expParam.ptdCustom.view = $(e.target).val();
                                that.modelInputPtdChange();
                            });
                        }
                    }

                    if(typeof this.getClass() === 'string' && this.getClass().indexOf('datepicker') !== -1) {
                        var $dp = $(this.$el).find('.datepicker');
                        if ($dp.length > 0) {
                            $dp.datepicker({
                                endDate: new Date(),
                                weekStart: 1,
                                language: 'ru',
                                format: 'dd.mm.yyyy',
                                autoclose: true
                            }).on('changeDate', function (e) {
                                var val = $(e.target).val();
                                var val_r = val.split(".");
                                var val_r_y = parseInt(val_r[2]);
                                var val_r_m = parseInt(val_r[1]) - 1;
                                var val_r_d = parseInt(val_r[0]);
                                var curDate = new Date(val_r_y, val_r_m, val_r_d);
                                if (val_r_y >= 1980 && val_r_y <= 2100 && curDate.getFullYear() === val_r_y && curDate.getMonth() === val_r_m && curDate.getDate() === val_r_d) {
                                    that.item.expParam.ptdCustom.view = $(e.target).val();
                                    that.modelInputPtdChange();
                                }
                            });
                        }
                    }

                },
                methods: {

                    getSelf: function () {
                        return this;
                    },

                    getAttributes: function (attrname) {
                        if (this.item.outputMapping === undefined) {
                            return null;
                        }
                        if (this.item.outputMapping.attributes === undefined || this.item.outputMapping.attributes === null) {
                            return null;
                        }
                        if (this.item.outputMapping.attributes[attrname] === undefined) {
                            return null;
                        }

                        return this.item.outputMapping.attributes[attrname];
                    },

                    getClass: function () {
                        return this.getAttributes('class');
                    },

                    outputOnClick: function () {
                        var clickMethod = this.getAttributes('clickMethod');
                        if (clickMethod === 'createLicenseShowModal') {
                            this.createLicenseShowModal();
                        } else if (clickMethod === 'createOilfieldShowModal') {
                            this.createOilfieldShowModal();
                        } else if (clickMethod === 'deleteOilfield') {
                            this.deleteOilfield();
                        } else if (clickMethod === 'deleteLicense') {
                            this.deleteLicense();
                        } else {
                            console.log('not found clickMethod :' + clickMethod);
                        }
                    },

                    sendBaseChangeEvent: function (data) {
                        myVueWrapper.app.$emit('base-change-event', {
                            method: 'sendEventChange',
                            data: data
                        });
                    },

                    loadAndShowModalExpensesAdd: function () {
                        myVueWrapper.app.$emit('load-show-expenses-modal', {
                            data: {
                                parentOutputMappingId: this.item.outputMapping.id,
                            }
                        });
                    },

                    createLicenseShowModal: function () {
                        this.sendBaseChangeEvent({
                            sendEventName: 'createLicense',
                            slug: prototypeMark1TopPanelSlug,
                        });
                    },

                    createOilfieldShowModal: function () {
                        this.sendBaseChangeEvent({
                            sendEventName: 'createOilField',
                            slug: prototypeMark1TopPanelSlug,
                        });
                    },

                    deleteOilfield: function () {
                        this.sendBaseChangeEvent({
                            sendEventName: 'deleteOilfield',
                            customEntityId: this.item.customEntityItemId,
                            slug: prototypeMark1TopPanelSlug,
                        });
                    },

                    deleteLicense: function () {
                        this.sendBaseChangeEvent({
                            sendEventName: 'deleteLicense',
                            customEntityId: this.item.customEntityItemId,
                            slug: prototypeMark1TopPanelSlug,
                        });
                    },

                    deleteOutput: function () {
                        var item = this.item.output.id;
                        sweetAlert({
                            title: "Удалить показатель?",
                            showCancelButton: true,
                            confirmButtonColor: "#c9290a",
                            confirmButtonText: "Да, удалить!",
                            cancelButtonText: "Отмена",
                            cancelButtonColor: "#173242",
                            closeOnConfirm: true
                        }).then(
                            function (data) {
                                if(data.value === true) {
                                    myVueWrapper.app.$emit('base-change-event', {
                                        method: 'sendEventChange',
                                        data: {
                                            sendEventName: 'deleteOutput',
                                            outputId: item,
                                            slug: prototypeMark1TopPanelSlug
                                        }
                                    });
                                }
                            },
                            function () { return false; });
                    },

                    expParamChange: function (typeSlug, expParamId, value) {
                        myVueWrapper.app.$emit('base-change-event', {
                            method: 'expParamChange',
                            data: {
                                typeSlug: typeSlug,
                                expParamId: expParamId,
                                newValue: value,
                            }
                        });
                    },

                    modelInputPtdChange: function () {
                        this.model.slugSelectedType = 'ptdCustom';
                        this.$forceUpdate();
                        this.setInputSelected(this.item.expParam.ptdCustom.view);
                    },

                    modelInputSelectedChange: function () {
                        this.model.slugSelectedType = 'custom';
                        this.$forceUpdate();
                        this.setInputSelected(this.model.inputSelected);
                    },

                    modelInputCustomChange: function () {
                        this.model.slugSelectedType = 'custom';
                        this.$forceUpdate();
                        this.setInputSelected(this.item.expParam.custom.view);
                    },

                    setInputSelected: function (newValue) {
                        this.expParamChange(this.model.slugSelectedType, this.item.expParam.id, newValue);
                        this.model.inputSelected = newValue;
                    },


                    refrashDanger: function () {
                        this.model.isDanger = false;
                        this.model.isWarning = false;

                        if (this.item.expParam.paramErrorType === 2) {
                            this.model.isDanger = true;
                            return true;
                        }

                        if (this.item.expParam.selectedType === 4 && this.item.expParam.ptdCustom !== undefined) { //ptdCustom
                            if (this.item.expParam.ptdCustom.error > 1) {
                                this.model.isDanger = true;
                                return true;
                            }
                        }

                        if (this.item.expParam.selectedType === 3) { //ptdCustom
                            if (this.item.expParam.custom.error > 1) {
                                this.model.isDanger = true;
                                return true;
                            }
                        }

                        if (this.item.expParam.paramErrorType === 1) {
                            this.model.isWarning = true;
                            return true;
                        }

                        return false;
                    },

                    setSlugSelectedType: function (slug) {
                        this.model.slugSelectedType = slug;
                        if (slug === 'ptdCustom') {
                            this.setInputSelected(this.item.expParam.ptdCustom.view);
                            this.$forceUpdate();
                        } else if (slug === 'recommend') {
                            this.setInputSelected(this.item.expParam.recommend.view);
                            this.$forceUpdate();
                        }
                    },

                    refrash: function () {
                        if (this.item.expParam != undefined) {

                            if (this.item.expParam.selectedType === 4 ) { //ptdCustom
                                if(this.item.expParam.ptdCustom !== undefined) {
                                    if (this.item.expParam.ptdCustom.error > 0) {
                                        this.item.expParam.ptdCustom.view = '?';
                                    }
                                    this.model.inputSelected = this.item.expParam.ptdCustom.view;
                                } else {
                                    this.model.inputSelected = '';
                                }
                                this.model.slugSelectedType = 'ptdCustom';
                            }

                            if (this.item.expParam.selectedType === 2) {
                                if(this.item.expParam.recommend !== undefined) {
                                    this.model.slugSelectedType = 'recommend';
                                    this.model.inputSelected = this.item.expParam.recommend.view;
                                }
                            }

                            if (this.item.expParam.selectedType === 3) {
                                if (this.item.expParam.custom.error > 0) {
                                    this.item.expParam.custom.view = '?';
                                }
                                this.model.slugSelectedType = 'custom';
                                this.model.inputSelected = this.item.expParam.custom.view;
                            }

                            this.refrashDanger();
                        }

                        componentBaseObj.refrashCustom(this);
                    },
                }
            };

            componentBaseObj.refrashCustom = function () {};

            return componentBaseObj;
        }}
    });